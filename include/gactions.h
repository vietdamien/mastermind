/* *****************************************************************************
 * Project name: Mastermind
 * File name   : gactions
 * Author      : Damien Nguyen
 * Date        : Monday, November 18 2019
 * ****************************************************************************/

#ifndef MASTERMIND_GRAPHICAL_ACTIONS_H
#define MASTERMIND_GRAPHICAL_ACTIONS_H

// ========================== CONSTANTS AND MACROS ========================== //


// =============================== STRUCTURES =============================== //


// ============================== DECLARATIONS ============================== //
int graphicalRun(int argc, const char **argv);

#endif // MASTERMIND_GRAPHICAL_ACTIONS_H
