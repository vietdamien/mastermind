/* *****************************************************************************
 * Project name: Mastermind
 * File name   : cactions
 * Author      : Damien Nguyen
 * Date        : Saturday, October 26 2019
 * ****************************************************************************/

#ifndef MASTERMIND_CONSOLE_ACTIONS_H
#define MASTERMIND_CONSOLE_ACTIONS_H

#include <stdbool.h>

#include "mastermind.h"

// =============================== PROTOTYPES =============================== //
/**
 * Displays a list of actions and returns the user's choice.
 *
 * @return the user's choice
 */
int menu(void);

/**
 * Calls the action the user chose.
 *
 * @param choice the user's choice
 */
void handle(unsigned int choice);

/**
 * Launches a Mastermind game.
 */
void play(void);

/**
 * Prompts the user to make their proposition.
 *
 * @param proposition the array that stores the user's proposition
 */
void promptProposition(int *proposition);

/**
 * Runs the console version of the game.
 *
 * @param argc the command line arguments count
 * @param argv the command line arguments array
 * @return the exit status
 */
int consoleRun(int argc, const char **argv);

#endif //MASTERMIND_CONSOLE_ACTIONS_H
