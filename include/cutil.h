/* *****************************************************************************
 * Project name: Mastermind
 * File name   : utilities
 * Author      : Damien Nguyen
 * Date        : Sunday, October 27 2019
 * ****************************************************************************/

#ifndef MASTERMIND_CONSOLE_UTIL_H
#define MASTERMIND_CONSOLE_UTIL_H

#include <stdbool.h>
#include <time.h>

#include "mastermind.h"

// ========================== CONSTANTS AND MACROS ========================== //
#define RESET_INDEX 0

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define CHAR_EOL      '\0'
#define CHAR_NEW_LINE '\n'

#define CLEAR_SCREEN "\e[1;1H\e[2J"

#define DEFAULT_BASE 10

#define STR_EMPTY    ""
#define STR_NEW_LINE "\n"

// =============================== PROTOTYPES =============================== //
/**
 * Checks whether the given color index is valid (between 0 and 6)
 *
 * @param index the given color index
 * @return true if it is, false otherwise
 */
bool isValidColor(unsigned int index);

/**
 * Updates the "r"th line of the given board by comparing the given proposition
 * to the given solution.
 *
 * @param p the given proposition
 * @param s to the given solution
 * @param r the "r"th line which is the round number
 * @return true if the proposition matches the solution, false otherwise
 */
bool updateBoard(int (*)[NB_COLUMNS + 2], const int *p, const int *s, int r);

/**
 * Gets the ANSI color string associated to the given index.
 *
 * @param index the given index
 * @return the associated ANSI color string
 */
char *getColor(unsigned int index);

/**
 * Initializes the random number generator with the given seed.
 *
 * @param seed the given seed
 */
void initRandGenerator(time_t *seed);

/**
 * Shows the state of the game board with the user's propositions and the number
 * of guessed colors and the number of misplaced colors.
 *
 * @param board the game board
 * @param round the ongoing round
 */
void showBoard(const int (*board)[NB_COLUMNS + 2], int round);

/**
 * Shows the colored board line.
 *
 * @param line the original board line
 */
void showColoredBoardLine(const int *line);

/**
 * Shows all the available colors.
 */
void showColors(void);

/**
 * Shows the solution.
 *
 * @param solution the solution
 */
void showSolution(const int *solution);

/**
 * Shows the final game state.
 *
 * @param wasCompleted indicates whether the user guessed the combination
 * @param solution     the solution
 */
void showState(bool wasCompleted, const int *solution);

#endif // MASTERMIND_CONSOLE_UTIL_H
