/* *****************************************************************************
 * Project name: Mastermind
 * File name   : gio
 * Author      : Damien Nguyen
 * Date        : Monday, November 18 2019
 * ****************************************************************************/

#ifndef MASTERMIND_GRAPHICAL_IO_H
#define MASTERMIND_GRAPHICAL_IO_H

#include <SDL.h>
#include <SDL_render.h>
#include <SDL_video.h>

#include "gio.h"

// ========================== CONSTANTS AND MACROS ========================== //
#define WINDOW_HEIGHT 720
#define WINDOW_WIDTH  1280

// =============================== STRUCTURES =============================== //
typedef struct {
    SDL_Event    event;
    SDL_Renderer *renderer;
    SDL_Window   *window;
} view_t;

// ============================== DECLARATIONS ============================== //
view_t *initView(void);

void destroyView(view_t *view);

#endif // MASTERMIND_GRAPHICAL_IO_H
