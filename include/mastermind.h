/* *****************************************************************************
 * Project name: Mastermind
 * File name   : mastermind
 * Author      : Damien Nguyen
 * Date        : Thursday, October 17 2019
 * ****************************************************************************/

#ifndef MASTERMIND_MASTERMIND_H
#define MASTERMIND_MASTERMIND_H

#include <stdbool.h>

// ========================== CONSTANTS AND MACROS ========================== //
#define APP_NAME "Mastermind"

#define EMPTY_CELL 0
#define NB_COLORS  6
#define NB_COLUMNS 4
#define NB_LINES   10

#define UNIT "■"

// =============================== STRUCTURES =============================== //
typedef struct {
    int guessed;
    int misplaced;
} combination_t;

// =============================== PROTOTYPES =============================== //
/**
 * Checks whether a combination was found (the number of guessed colors should
 * equal the number and columns and the number of misplaced colors should equal
 * 0.
 *
 * @return true if it was, false otherwise
 */
bool combinationFound(combination_t);

/**
 * Fills a combination with the number of guessed colors and the number of
 * misplaced colors from the given proposition which is compared to the given
 * solution.
 *
 * @param proposition the given proposition
 * @param solution    the given solution
 * @return a combination with information on the given proposition
 */
combination_t evaluate(const int *proposition, const int *solution);

/**
 * Initializes the given game board with zeros.
 *
 * @param board the given game board
 */
void initBoard(int (*board)[NB_COLUMNS + 2]);

/**
 * Initializes the given solution with random colors.
 *
 * @param solution the given solution
 */
void initSolution(int *solution);

#endif // MASTERMIND_MASTERMIND_H
