/* *****************************************************************************
 * Project name: Mastermind
 * File name   : io
 * Author      : Damien Nguyen
 * Date        : Saturday, October 26 2019
 * ****************************************************************************/

#ifndef MASTERMIND_CONSOLE_IO_H
#define MASTERMIND_CONSOLE_IO_H

#include <stdio.h>

// ========================== CONSTANTS AND MACROS ========================== //
#define STR_LEN 1024

#define ESCAPE_PATTERN "%*c"
#define STR_PATTERN    "%s"

// =============================== PROTOTYPES =============================== //
/**
 * Prompts the user to input a string.
 *
 * @param msg the prompt message
 * @return the user's input string
 */
char *readLine(const char *msg);

/**
 * Prompts the user to input a number (all verifications are done in this
 * function.
 *
 * The result will have to be cast if needed.
 *
 * @param msg the prompt message
 * @return the user's input number
 */
double readNumber(const char *msg);

/**
 * Reads a string with the given maximum length of characters from the given
 * input file and stores it into the destination character array.
 *
 * @param in the given input file
 * @param dst the destination character array
 * @param len the given maximum length
 * @return the length of the actually read string INCLUDING the '\n'
 */
size_t readStr(FILE *in, char *dst, size_t len);

#endif // MASTERMIND_CONSOLE_IO_H
