/* *****************************************************************************
 * Project name: Mastermind
 * File name   : mastermind
 * Author      : Damien Nguyen
 * Date        : Thursday, October 17 2019
 * ****************************************************************************/

#include <stdlib.h>

#include "mastermind.h"

typedef combination_t info;

bool _contains(const int *const array, int value) {
    int i;

    for (i = 0; i < NB_COLUMNS; ++i) {
        if (array[i] == value) { return true; }
    }

    return false;
}

void _guessedIndices(combination_t *combination, const int *const proposition,
                     const int *const solution, int *indices) {
    int i;

    for (i = 0; i < NB_COLUMNS; ++i) {
        if (proposition[i] == solution[i]) {
            indices[combination->guessed++] = i;
        }
    }
}

bool combinationFound(combination_t combination) {
    return combination.guessed == NB_COLUMNS && !combination.misplaced;
}

info evaluate(const int *const proposition, const int *const solution) {
    info combination         = { 0, 0 };
    int  indices[NB_COLUMNS] = { EMPTY_CELL }, i, j;

    _guessedIndices(&combination, proposition, solution, indices);

    for (i = 0; i < NB_COLUMNS; ++i) {
        if (_contains(solution, proposition[i])) {
            bool guessed = false;

            for (j = 0; j < combination.guessed; ++j) {
                guessed = proposition[i] == solution[indices[j]] || guessed;
            }
            if (!guessed) { ++combination.misplaced; }
        }
    }

    return combination;
}

void initBoard(int (*const board)[NB_COLUMNS + 2]) {
    int i, j;

    for (i = 0; i < NB_LINES; ++i) {
        for (j = 0; j < NB_COLUMNS + 2; ++j) {
            board[i][j] = EMPTY_CELL;
        }
    }
}

void initSolution(int *const solution) {
    int i;

    for (i = 0; i < NB_COLUMNS; ++i) {
        solution[i] = (rand() % NB_COLORS) + 1;
    }
}
