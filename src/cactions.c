/* *****************************************************************************
 * Project name: Mastermind
 * File name   : cactions
 * Author      : Damien Nguyen
 * Date        : Saturday, October 26 2019
 * ****************************************************************************/

#include <stdlib.h>

#include "cactions.h"
#include "cio.h"
#include "cutil.h"

static void (*const userActions[])(void) = {
        NULL,
        play,
};

void handle(const unsigned int choice) {
    if ((*userActions[choice])) {
        (*userActions[choice])();
    }
}

int menu(void) {
    printf("===== MENU =====\n");
    printf("1 - Play\n");
    printf("* - Quit\n");

    return (int) readNumber("Choice: ");
}

void play(void) {
    bool wasFound = false;
    int  round    = 0, board[NB_LINES][NB_COLUMNS + 2];
    int  proposition[NB_COLUMNS], solution[NB_COLUMNS];

    initBoard(board);
    initSolution(solution);

    showSolution(solution); // DEBUG
    showColors();

    while (round < NB_LINES && !wasFound) {
        promptProposition(proposition);
        wasFound = updateBoard(board, proposition, solution, ++round);
        showBoard(board, round);
    }

    showState(wasFound && round < NB_LINES, solution);
}

void promptProposition(int *const proposition) {
    char *str, *stopStr;
    int  arr[NB_COLUMNS], i;

    str     = readLine("Proposition (digits separated by a single space): ");
    stopStr = str;

    for (i = 0; i < NB_COLUMNS; ++i) {
        arr[i] = (int) strtod(stopStr, &stopStr);
        if (!isValidColor(arr[i])) {
            fprintf(stderr, "Value #%d is invalid, setting no color.\n", i + 1);
            arr[i] = EMPTY_CELL;
        }
    }

    for (i = 0; i < NB_COLUMNS; ++i) { proposition[i] = arr[i]; }

    free(str);
}

int consoleRun(int argc, const char **argv) {
    int    choice;
    time_t seed;

    if (argc != 2) {
        initRandGenerator(NULL);
        seed = rand();
        fprintf(stderr, "No arg: seed set to %lu.\n", seed);
    } else {
        seed = strtoul(argv[1], NULL, DEFAULT_BASE);
    }

    initRandGenerator(&seed);

    do { handle(choice = menu()); } while (choice == 1);

    return EXIT_SUCCESS;
}
