/* *****************************************************************************
 * Project name: Mastermind
 * File name   : gio
 * Author      : Damien Nguyen
 * Date        : Monday, November 18 2019
 * ****************************************************************************/

#include "gio.h"
#include "mastermind.h"

view_t *initView(void) {
    view_t *view = NULL;

    if (!(view = (view_t *) malloc(sizeof(view_t)))) {
        perror("initView: view allocation problem.\n");
        goto Exit; // NULL tested in calling function
    }

    view->window   = SDL_CreateWindow(
            APP_NAME, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN
    );
    view->renderer = SDL_CreateRenderer(
            view->window, -1, SDL_RENDERER_ACCELERATED
    );

    if (!view->window || !view->renderer) {
        fprintf(stderr, "initView: problem creating the window or renderer.\n");
        free(view);
        view = NULL;
        goto Exit;
    }

    Exit:
    return view;
}

void destroyView(view_t *view) {
    SDL_DestroyWindow(view->window);
    SDL_DestroyRenderer(view->renderer);
    free(view);
}
