/* *****************************************************************************
 * Project name: Mastermind
 * File name   : gactions
 * Author      : Damien Nguyen
 * Date        : Monday, November 18 2019
 * ****************************************************************************/

#include <stdlib.h>
#include <SDL.h>
#include <stdbool.h>

#include "gactions.h"
#include "gio.h"

int graphicalRun(int argc, const char **argv) {
    bool   running = true;
    int    status  = EXIT_FAILURE;
    view_t *view;

    if (SDL_Init(SDL_INIT_VIDEO)) {
        fprintf(stderr, "SDL_Init error: %s\n", SDL_GetError());
        goto Exit;
    }

    view = initView();

    //region Tests
    SDL_SetRenderDrawColor(view->renderer, 0, 0, 0, 0);
    SDL_RenderClear(view->renderer);
    SDL_Rect rect;
    rect.x = rect.y = 0;
    rect.w = rect.h = 100;
    SDL_SetRenderDrawColor(view->renderer, 255, 255, 255, 0);
    SDL_RenderFillRect(view->renderer, &rect);
    SDL_RenderPresent(view->renderer);
    //endregion

    while (running) {
        while (SDL_PollEvent(&view->event)) {
            switch (view->event.type) {
                case SDL_QUIT:
                    goto End;
                default:
                    break;
            }
        }
    }

    End:
    destroyView(view);

    status = EXIT_SUCCESS;
    Exit:
    return status;
}
