/* *****************************************************************************
 * Project name: Mastermind
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Thursday, October 24 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "cactions.h"
#include "cutil.h"
#include "gactions.h"

static int (*const runFunc[])(int, const char **) = {
        consoleRun,
        graphicalRun
};

int main(int argc, const char **argv) {
    int arg, status = EXIT_FAILURE;

    if (argc != 2) {
        fprintf(stderr, "Usage: ./mastermind <version>\n");
        goto Exit;
    }

    if ((*runFunc[(arg = (int) strtod(argv[1], NULL))])) {
        status = (*runFunc[arg])(argc, argv);
    }

    Exit:
    return status;
}
