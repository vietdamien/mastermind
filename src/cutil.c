/* *****************************************************************************
 * Project name: Mastermind
 * File name   : utilities
 * Author      : Damien Nguyen
 * Date        : Sunday, October 27 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <cio.h>

#include "cutil.h"
#include "mastermind.h"

static char *colors[] = {
        ANSI_COLOR_RESET,
        ANSI_COLOR_RED,
        ANSI_COLOR_GREEN,
        ANSI_COLOR_YELLOW,
        ANSI_COLOR_BLUE,
        ANSI_COLOR_MAGENTA,
        ANSI_COLOR_CYAN,
};

bool isValidColor(const unsigned int index) {
    return index > 0 && index <= NB_COLORS;
}

bool updateBoard(int (*board)[NB_COLUMNS + 2], const int *const proposition,
                 const int *const solution, int round) {
    combination_t combination = evaluate(proposition, solution);

    int i;

    for (i = 0; i < NB_COLUMNS; ++i) { board[round - 1][i] = proposition[i]; }

    board[round - 1][NB_COLUMNS]     = combination.guessed;
    board[round - 1][NB_COLUMNS + 1] = combination.misplaced;

    return combinationFound(combination);
}

char *getColor(const unsigned int index) {
    return colors[index];
}

void initRandGenerator(time_t *seed) {
    srand(time(seed));
}

void showColors(void) {
    int i;

    printf("Available colors: ");
    fflush(stdout);
    for (i = 1; i <= NB_COLORS; ++i) {
        char *color = getColor(i);
        printf("%s(%d) " ANSI_COLOR_RESET, color, i);
        fflush(stdout);
    }
    printf("\n");
}

void _printCombination(const int *const line) {
    int i;

    for (i = 0; i < NB_COLUMNS; ++i) {
        printf("%s %s %s", getColor(line[i]), UNIT, getColor(RESET_INDEX));
    }
}

void _printPlacements(const int *const line) {
    printf(ANSI_COLOR_GREEN "%3d" ANSI_COLOR_RESET, line[NB_COLUMNS]);
    printf(ANSI_COLOR_RED   "%3d" ANSI_COLOR_RESET, line[NB_COLUMNS + 1]);
}

void showBoard(const int (*const board)[NB_COLUMNS + 2], int round) {
    int i;

    for (i = 0; i < round; ++i) {
        showColoredBoardLine(board[i]);
    }

    printf(STR_NEW_LINE);
}

void showColoredBoardLine(const int *const line) {
    _printCombination(line);
    printf("%3s", STR_EMPTY);
    _printPlacements(line);
    printf(STR_NEW_LINE);
}

void showSolution(const int *const solution) {
    printf("Solution: ");
    _printCombination(solution);
    printf(STR_NEW_LINE);
}

void showState(bool wasCompleted, const int *const solution) {
    if (!wasCompleted) {
        printf(ANSI_COLOR_RED "YOU LOST\n" ANSI_COLOR_RESET);
        showSolution(solution);
    } else {
        printf(ANSI_COLOR_GREEN "YOU WON!\n" ANSI_COLOR_RESET);
    }

    printf(ANSI_COLOR_YELLOW "Press ENTER to continue...\n" ANSI_COLOR_RESET);
    scanf(ESCAPE_PATTERN);
    printf(CLEAR_SCREEN);
}
